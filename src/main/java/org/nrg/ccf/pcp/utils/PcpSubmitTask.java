package org.nrg.ccf.pcp.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.nrg.ccf.pcp.dto.PcpSubmitItem;
import org.nrg.ccf.pcp.inter.PcpCondensedStatusI;
import org.nrg.ccf.pcp.inter.PipelineExecManagerI;
import org.nrg.ccf.pcp.inter.PipelineSubmitterI;
import org.nrg.ccf.pcp.inter.PipelineValidatorI;
import org.nrg.ccf.pcp.runner.PcpPipelineRunner;
import org.nrg.ccf.pcp.services.PcpExecService;
import org.nrg.xdat.entities.AliasToken;
import org.nrg.xdat.services.AliasTokenService;
import org.nrg.xdat.preferences.SiteConfigPreferences;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.node.entities.XnatNodeInfo;
import org.nrg.xnat.node.services.XnatNodeInfoService;
import org.nrg.xnat.task.AbstractXnatTask;
import org.nrg.framework.task.XnatTask;
import org.nrg.framework.task.services.XnatTaskService;
import org.nrg.prefs.entities.Preference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

@XnatTask(taskId = "PcpSubmitTask", description = "Pipeline Control Panel Submit Task",
		defaultExecutionResolver = "SingleNodeExecutionResolver", executionResolverConfigurable = false)
public class PcpSubmitTask extends AbstractXnatTask {

	private static final Logger _logger = LoggerFactory.getLogger(PcpSubmitTask.class);
	private String _projectId;
	private String _pipelineId;
	private PipelineExecManagerI _execManager;
	private PipelineSubmitterI _submitter;
	private PipelineValidatorI _validator;
	private PcpSubmitItem _submitItem;
	private List<PcpCondensedStatusI> _statusList;
	private Map<String, String> _parameters;
	private UserI _user;
	private SiteConfigPreferences _preferences;
	private XnatNodeInfoService _nodeInfoService;
	private AliasTokenService _tokenService;
	private boolean _forceProcessLocally;

	public PcpSubmitTask(String projectId, String pipelineId, PipelineExecManagerI execManager, PipelineSubmitterI submitter, PipelineValidatorI validator,
			PcpSubmitItem submitItem, List<PcpCondensedStatusI> statusList, Map<String, String> parameters, UserI user, XnatTaskService taskService,
			SiteConfigPreferences preferences, XnatNodeInfoService nodeInfoService, AliasTokenService tokenService, 
			Boolean forceProcessLocally) {
		super(taskService);
		_projectId = projectId;
		_pipelineId = pipelineId;
		_execManager = execManager;
		_submitter = submitter;
		_validator = validator;
		_submitItem = submitItem;
		_statusList = statusList;
		_parameters = parameters;
		_user = user;
		_preferences = preferences;
		_nodeInfoService = nodeInfoService;
		_tokenService = tokenService;
		_forceProcessLocally = forceProcessLocally;
		
	}

	public void submit() {
		
		if (_forceProcessLocally || shouldRunTask()) {
			if (_forceProcessLocally) {
				_logger.debug("PCPSubmitTask - requesting local processing");
			}
			doSubmit();
		} else {
			_logger.info("This node is not configured to submit PCP pipelines.  Submitting to configured node.");
			// Determine submit node
			try {
				final String prefKey = "task-PcpSubmitTask-node";
				if (_preferences != null && _preferences.containsKey(prefKey)) {
					final Preference taskNodePref = _preferences.getPreference(prefKey);
					final String taskVal = taskNodePref.getValue();
					if (taskVal!=null && taskVal.length()>0) {
						if (_nodeInfoService != null) {
							for (XnatNodeInfo nodeInfo : _nodeInfoService.getAll()) {
								if (nodeInfo.getNodeId().equals(taskVal)) {
									// TODO:  Do we want to implement any token management here?  Currently creating a new one 
									// for each submit.
									final String submitHost = nodeInfo.getHostName();
									final SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();
									final RestTemplate restTemplate = new RestTemplate(requestFactory);
									final HttpHeaders headers = new HttpHeaders();
									final AliasToken token = _tokenService.issueTokenForUser(_user, true);
									final String plainCreds = token.getAlias() + ":" + token.getSecret();
									final byte[] plainCredsBytes = plainCreds.getBytes();
									final byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
									final String base64Creds = new String(base64CredsBytes);
									headers.add("Authorization",  "Basic " + base64Creds);
									final HttpEntity<PcpSubmitItem> httpEntity = new HttpEntity<PcpSubmitItem>(_submitItem, headers);
									String qualHost;
									try {
										final InetAddress[] addrs = InetAddress.getAllByName(submitHost);
										for (final InetAddress addr : Arrays.asList(addrs)) {
											qualHost = addr.getCanonicalHostName();
											final boolean rc = tryPost(qualHost, restTemplate, httpEntity);
											if (rc) return;
										}
									} catch (UnknownHostException e) {
										qualHost = submitHost;
										final boolean rc = tryPost(qualHost, restTemplate, httpEntity);
										if (rc) return;
									}
								}
							}
						}
					}
				}
			} catch (Exception e) {
				_logger.error("PCPSubmitTask - Exception thrown trying remote submit - ", e);
			}
			_logger.error("ERROR:   Could not find or execute on configured submit node.  Executing locally.");
			doSubmit();
		}
	}
	
	private boolean tryPost(final String qualHost, final RestTemplate restTemplate, final HttpEntity<PcpSubmitItem> httpEntity) {
		try {
			final String submitUrl = "https://" + qualHost + "/xapi/pipelineControlPanel/project/" +
					_projectId + "/pipeline/" + _pipelineId + "/pipelineSubmit?forceProcessLocally=true";
			final ResponseEntity<String> response = restTemplate.exchange(submitUrl, HttpMethod.POST,
				httpEntity, String.class);
			if (response.getStatusCode().equals(HttpStatus.OK)) {
				_logger.info("Successfully submitted processing for (PROJECT=" + _projectId + ", PIPELINE=" + _pipelineId + " to " +
						qualHost);
				return true;
			}
		} catch (Exception e) {
			_logger.debug("Debugging exception - ", e);
		}
		return false;
	}

	private void doSubmit() {
		_logger.info("Submitting processing for (PROJECT=" + _projectId + ", PIPELINE=" + _pipelineId + ") to " +
					"the local machine.");
		final PcpPipelineRunner runner = new PcpPipelineRunner(_execManager, _submitter, _validator, _statusList,
				_parameters, _user);
		PcpExecService.execute(runner);
	}

}
