package org.nrg.ccf.pcp.constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PcpConstants {

	public enum PcpStatus { NOT_SUBMITTED, SUBMITTED, QUEUED, RUNNING, COMPLETE, ERROR, EXT_COMPLETE, REMOVED, RESET, CANCELLED, UNKNOWN }
	
	public static final List<String> statusList = new ArrayList<>();
	
	static {
		PcpStatus[] statusValues = PcpStatus.values();
		for (PcpStatus status : Arrays.asList(statusValues)) {
			statusList.add(status.toString());
		}
	}
	

}
