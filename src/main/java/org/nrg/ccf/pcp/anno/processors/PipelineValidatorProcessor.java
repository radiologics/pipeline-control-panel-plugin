package org.nrg.ccf.pcp.anno.processors;

import org.kohsuke.MetaInfServices;
import com.google.common.collect.Maps;

import org.nrg.ccf.pcp.anno.PipelineValidator;
import org.nrg.framework.processors.NrgAbstractAnnotationProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.TypeElement;
import java.util.Map;

@MetaInfServices(Processor.class)
@SupportedAnnotationTypes("org.nrg.ccf.pcp.anno.PipelineValidator")
public class PipelineValidatorProcessor extends NrgAbstractAnnotationProcessor<PipelineValidator> {
	
	@Override
	protected Map<String, String> processAnnotation(TypeElement element, PipelineValidator annotation) {
		final Map<String, String> properties = Maps.newLinkedHashMap();
		properties.put(PipelineValidator.PIPELINE_VALIDATOR, element.getQualifiedName().toString());
		return properties;
	}

	@Override
	protected String getPropertiesName(TypeElement element, PipelineValidator annotation) {
        return String.format("pcp/%s-pcp.properties", element.getSimpleName());
	}

}
