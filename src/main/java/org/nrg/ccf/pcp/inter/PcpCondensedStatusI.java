package org.nrg.ccf.pcp.inter;

public interface PcpCondensedStatusI extends Comparable<PcpCondensedStatusI> {

	String getProject();
	void setProject(String project);
	String getPipeline();
	void setPipeline(String pipeline);
	String getEntityType();
	void setEntityType(String entityType);
	String getEntityId();
	void setEntityId(String entityId);
	String getEntityLabel();
	void setEntityLabel(String entityLabel);
	String getSubGroup();
	void setSubGroup(String subGroup);
	String getStatus();
	void setStatus(String status);
	Boolean getPrereqs();
	void setPrereqs(Boolean prereqs);
	Boolean getValidated();
	void setValidated(Boolean validated);
	Boolean getIssues();
	void setIssues(Boolean issues);
	Boolean getImpeded();
	void setImpeded(Boolean impeded);

}
