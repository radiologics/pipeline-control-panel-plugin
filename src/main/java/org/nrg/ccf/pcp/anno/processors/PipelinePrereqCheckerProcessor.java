package org.nrg.ccf.pcp.anno.processors;

import org.kohsuke.MetaInfServices;
import com.google.common.collect.Maps;

import org.nrg.ccf.pcp.anno.PipelinePrereqChecker;
import org.nrg.framework.processors.NrgAbstractAnnotationProcessor;
import javax.annotation.processing.Processor;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.lang.model.element.TypeElement;
import java.util.Map;

@MetaInfServices(Processor.class)
@SupportedAnnotationTypes("org.nrg.ccf.pcp.anno.PipelinePrereqChecker")
public class PipelinePrereqCheckerProcessor extends NrgAbstractAnnotationProcessor<PipelinePrereqChecker> {
	
	@Override
	protected Map<String, String> processAnnotation(TypeElement element, PipelinePrereqChecker annotation) {
		final Map<String, String> properties = Maps.newLinkedHashMap();
		properties.put(PipelinePrereqChecker.PIPELINE_PREREQ_CHECKER, element.getQualifiedName().toString());
		return properties;
	}

	@Override
	protected String getPropertiesName(TypeElement element, PipelinePrereqChecker annotation) {
        return String.format("pcp/%s-pcp.properties", element.getSimpleName());
	}

}
