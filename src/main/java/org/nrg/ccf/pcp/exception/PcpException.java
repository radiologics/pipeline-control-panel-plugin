package org.nrg.ccf.pcp.exception;

public class PcpException extends Exception {

	private static final long serialVersionUID = -1070235986810514032L;

	public PcpException() {
		super();
	}

	public PcpException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PcpException(String message, Throwable cause) {
		super(message, cause);
	}

	public PcpException(String message) {
		super(message);
	}

	public PcpException(Throwable cause) {
		super(cause);
	}
	

}
