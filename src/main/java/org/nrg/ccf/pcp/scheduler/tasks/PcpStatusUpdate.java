package org.nrg.ccf.pcp.scheduler.tasks;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import org.apache.log4j.Logger;
import org.nrg.ccf.pcp.constants.PcpConfigConstants;
import org.nrg.ccf.pcp.dto.PcpConfigInfo;
import org.nrg.ccf.pcp.exception.PcpComponentSetException;
import org.nrg.ccf.pcp.exception.PcpUpdateRunningException;
import org.nrg.ccf.pcp.preferences.PcpPreferences;
import org.nrg.ccf.pcp.services.PcpStatusUpdateService;
import org.nrg.config.entities.Configuration;
import org.nrg.config.services.ConfigService;
import org.nrg.framework.constants.Scope;
import org.nrg.framework.task.XnatTask;
import org.nrg.framework.task.services.XnatTaskService;
import org.nrg.xdat.om.XnatProjectdata;
import org.nrg.xdat.security.helpers.Roles;
import org.nrg.xdat.security.services.UserManagementServiceI;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.task.AbstractXnatTask;

import com.google.gson.reflect.TypeToken;

@XnatTask(taskId = "PcpStatusUpdate", description = "Pipeline Control Panel Status Update Task", 
	defaultExecutionResolver = "SingleNodeExecutionResolver", executionResolverConfigurable = true)
public class PcpStatusUpdate extends AbstractXnatTask implements Runnable {
	
	private final Map<String,Map<String,Integer>> runCache = new HashMap<>();
	private final Map<String,UserI> userCache = new HashMap<>();
	private static int _currentlyRunning = 0;
	private final PcpStatusUpdateService _statusUpdateService;
	private final PcpPreferences _pcpPreferences;
	private final UserManagementServiceI _userManagementService;
	public static Logger _logger = Logger.getLogger(PcpStatusUpdate.class);
	private final Gson _gson = new Gson();
	private ConfigService _configService;
	final RuntimeMXBean _mxBean = ManagementFactory.getRuntimeMXBean();
	
	public PcpStatusUpdate(final PcpStatusUpdateService statusUpdateService, final PcpPreferences pcpPreferences, final ConfigService configService,
			XnatTaskService taskService, UserManagementServiceI userManagementService) {
		super(taskService);
		_logger.debug("Instantiate PcpStatusUpdate");
		_statusUpdateService = statusUpdateService;
		_pcpPreferences = pcpPreferences;
		_configService = configService;
		_userManagementService = userManagementService;
	}
	
	@Override
	public void run() {
		_logger.debug("Run PcpStatusUpdate");
		if (_statusUpdateService == null) {
			_logger.error("Cannot run PCP status update.  Null value for PcpStatusUpdateService");
			return;
		}
		if (!shouldRunTask()) {
			_logger.info("PCP Status Update is not configured to run on this node.  Skipping.");
			return;
		}
		if (_currentlyRunning>=3) {
			_logger.info("Three PCP Status Update processes are already running.  This one will be skipped.");
			return;
		}
		try  {
			_currentlyRunning++;
			// CCF-326:  Let's not let this run if the web server has recently been started.  There were instances
			// where initial runs caused the status values to be set to REMOVED.  Presumably this can run when the 
			// site is only partially functional.
			final Long upTime = (_mxBean!=null) ? _mxBean.getUptime() : Long.MAX_VALUE;
			if (upTime<PcpConfigConstants.UP_TIME_WAIT) {
				_logger.info("Skipping PCP status update due to recent web server restart (UPTIME=" + upTime + "ms).");
				return;
			}
			for (final String projectId : _pcpPreferences.getPcpEnabledProjects()) {
				final Configuration conf = _configService.getConfig(PcpConfigConstants.CONFIG_TOOL, PcpConfigConstants.CONFIG_PATH, Scope.Project, projectId);
				if (conf==null) {
					_logger.error("ERROR:  Could not obtain configuration for " + PcpConfigConstants.CONFIG_TOOL + ".");
					return;
				}
				final List<String> pipelineList = new ArrayList<>();
		    	final List<PcpConfigInfo> configList = _gson.fromJson(conf.getContents(),new TypeToken<List<PcpConfigInfo>>(){}.getType());
		    	for (final PcpConfigInfo config : configList) {
		    		if (config==null || config.getPipeline()==null) {
		    			continue;
		    		}
		    		final String currPipeline = config.getPipeline();
		    		if (!pipelineList.contains(currPipeline)) {
		    			pipelineList.add(currPipeline);
		    			if (runThisTime(projectId, config)) {
		    				try {
		    					_logger.debug("Running update status per configuration (PROJECT=" + projectId + ", PIPELINE=" + currPipeline +
		    							", UPDATE_FREQUENCY=" + config.getUpdateFrequency() + ").");
		    					_statusUpdateService.updateProjectStatus(projectId, currPipeline, getProjectUser(projectId));
		    				} catch (PcpComponentSetException e) {
		    					_logger.error("ERROR:  PCP project status could not be updated (PROJECT=" + projectId + ", PIPELINE=" + currPipeline + ").");
		    				} catch (PcpUpdateRunningException e) {
		    					_logger.warn("Warning:  PCP update process already running - skipping this run (PROJECT=" + projectId + ", PIPELINE=" + currPipeline + ").");
							}
						} else {
		    				_logger.debug("Skipping status update run per configuration (PROJECT=" + projectId + ", PIPELINE=" + currPipeline +
		    						", UPDATE_FREQUENCY=" + config.getUpdateFrequency() + ").");
						}
		    		}
		    	}
			}
		} finally {
			_currentlyRunning--;
		}
	}

	private UserI getProjectUser(String projectId) {
		if (userCache.containsKey(projectId)) {
			return userCache.get(projectId);
		}
	    XnatProjectdata proj = XnatProjectdata.getXnatProjectdatasById(projectId, AdminUtils.getAdminUser(), false);
	    try {
			ArrayList<String> ownerList = proj.getOwners();
			for (final String owner : ownerList) {
				UserI user = _userManagementService.getUser(owner);
				if (user.isEnabled() && Roles.isSiteAdmin(user)) {
					return user;
				}
			}
			if (ownerList.size()>0) {
				return _userManagementService.getUser(ownerList.get(0));
			}
		} catch (Exception e) {
			// Do nothing
		}
		return AdminUtils.getAdminUser();
	}

	private boolean runThisTime(final String projectId, final PcpConfigInfo config) {
	    final String currPipeline = config.getPipeline();
	    boolean doRun = false;
		if (!runCache.containsKey(projectId)) {
			runCache.put(projectId, new HashMap<String, Integer>());
		}
		final Map<String, Integer> projectMap = runCache.get(projectId);
		if (!projectMap.containsKey(currPipeline)) {
			// Let's run on Tomcat startup or new configurations.
			doRun=true;
		} else {
			projectMap.put(currPipeline, projectMap.get(currPipeline)+1);
			final Integer iterations = projectMap.get(currPipeline);
			switch(config.getUpdateFrequency()) {
					case PcpConfigConstants.FREQUENCY_DISABLE_CACHING:
						// Doesn't need to be run by the scheduler.  Will be run each time status is requested.
						doRun=false;
						break;
					case PcpConfigConstants.FREQUENCY_15_MINUTES:
						if (iterations>=1) doRun=true;
						break;
					case PcpConfigConstants.FREQUENCY_30_MINUTES:
						if (iterations>=2) doRun=true;
						break;
					case PcpConfigConstants.FREQUENCY_HOURLY:
						if (iterations>=4) doRun=true;
						break;
					case PcpConfigConstants.FREQUENCY_3_HOURS:
						if (iterations>=12) doRun=true;
						break;
					case PcpConfigConstants.FREQUENCY_6_HOURS:
						if (iterations>=24) doRun=true;
						break;
					case PcpConfigConstants.FREQUENCY_12_HOURS:
						if (iterations>=48) doRun=true;
						break;
					case PcpConfigConstants.FREQUENCY_DAILY:
						if (iterations>=96) doRun=true;
						break;
					default:
						_logger.warn("Invalid value for PCP status update frequency (PROJECT=" + projectId + ", PIPELINE=" + currPipeline + ").");
						doRun=true;
						break;
			}
		}
		if (doRun) {
			projectMap.put(currPipeline, 0);
		}
		return doRun;
	}

}
